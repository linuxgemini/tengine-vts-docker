

<p align="center"><img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/320/apple/155/gear_2699.png" width="120px"></p>
<h1 align="center">Tengine (container image)</h1>
<p align="center">Built-from-source container image of the <a href="https://tengine.taobao.org/">Tengine</a> HTTP server, based on <a href="https://github.com/ricardbejarano/nginx">ricardbejarano/nginx</a></p>


## Tags

### GitLab Registry

Available on Docker Hub as [`registry.gitlab.com/linuxgemini/tengine-vts-docker`](https://gitlab.com/linuxgemini/tengine-vts-docker/container_registry):

- [`latest` *(Dockerfile)*](Dockerfile)


## Features

* Compiled from source during build time
* Built `FROM scratch`, with zero bloat
* Statically linked to the [`musl`](https://musl.libc.org/) implementation of the C standard library
* Reduced attack surface (no shell, no UNIX tools, no package manager...)
* Runs as unprivileged (non-`root`) user


## Building

```bash
docker build --tag linuxgemini/tengine-vts-docker --file Dockerfile .
```


## Configuration

### Volumes

- Mount your **configuration** at `/etc/nginx/nginx.conf`.


## License

MIT licensed, see [LICENSE](LICENSE) for more details.
